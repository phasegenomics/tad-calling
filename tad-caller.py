import sparse_to_dense
import plot_tads
import subprocess
import argparse
import logging
import json
import glob
import sys
import os

def setup():
	parser = argparse.ArgumentParser(description = 'Run TopDom')
	
	parser.add_argument(
		'-i', 
		'--input',
		metavar = 'File', 
		required = True, 
		help = 'JSON file with parameters'
	)

	return parser.parse_args()

def parse_chromosomes(species):
	if species.lower() == 'human':
		filename = 'defaults/human.txt'
	elif species.lower() == 'mouse':
		filename = 'defaults/human.txt'
	else:
		print ('Species not found, please make a chromosome size list in the \
			defaults folder.')
		sys.exit()

	path = os.path.dirname(os.path.realpath(__file__))

	with open(os.path.join(path, filename)) as infile:
		names = []
		lengths = [0]

		for line in infile:
			split = line.split()
			names.append(split[0])
			correction = int(split[1]) + lengths[-1]
			lengths.append(correction)

	return names, lengths

def run_juicer(infile, chromosome, start, end, resolution, outfile, path):
	logging.debug('Extracting data from .hic file')
	start_position = '{}:{}:{}'.format(chromosome, start, end)
	end_position = '{}:{}:{}'.format(chromosome, start, end)
	juicer_path = os.path.join(path, 'juicer_tools.jar')

	subprocess.call([
		'java', 
		'-jar', 
		juicer_path, 
		'dump', 
		'observed', 
		'KR', 
		infile,
		start_position, 
		end_position, 
		'BP', 
		str(resolution), 
		outfile])

	if os.path.getsize(outfile) == 0:
		subprocess.call([
			'java', 
			'-jar', 
			juicer_path, 
			'dump', 
			'observed', 
			'NONE', 
			infile,
			start_position, 
			end_position, 
			'BP', 
			str(resolution), 
			outfile])

def run_sparse_to_dense(infile, outfile, resolution, chromosome, start, end):
	matrix = sparse_to_dense.read(infile, resolution, start, end)
	sparse_to_dense.write(matrix, outfile, resolution, chromosome, start, end)

def run_topdom(infile, outfile, window, path):
	logging.debug('Running TopDom')
	current_directory = os.getcwd()
	os.chdir(path)

	devnull = open(os.devnull, 'w')

	subprocess.call([
		'Rscript', 
		'topdom-wrapper.R', 
		 os.path.join(current_directory, infile), 
		 os.path.join(current_directory, outfile), 
		 window],
		 stdout = devnull)

	os.chdir(current_directory)

def run_plot_tads(matrix, tad, resolution, figure, chromosome, start, end):
	
	matrix, domains = plot_tads.read(matrix, tad, resolution, start, end)
	plot_tads.plot(figure, matrix, domains, resolution, chromosome, start, end)

def cleanup(prefix, directory):
	logging.debug('Cleaning up files')

	sparse = os.path.join(directory, '{0}-sparse.txt'.format(prefix))
	os.remove(sparse)

	dense = os.path.join(directory, '{0}-dense.txt'.format(prefix))
	os.remove(dense)

	domain = os.path.join(directory, '*.domain'.format(prefix))
	for file in glob.glob(domain):
		os.remove(file)

	infile = os.path.join(directory, "*.bed")
	outfile = os.path.join(directory, "{0}.bed".format(prefix))

	subprocess.call('ls -v {0} | xargs cat > {1}'.format(infile, outfile), shell=True)

	bed = os.path.join(directory, '*tad.bed'.format(prefix))
	for file in glob.glob(bed):
		os.remove(file)

	infile = os.path.join(directory, "*.binSignal")
	outfile = os.path.join(directory, "{0}.binSignal".format(prefix))

	subprocess.call('ls -v {0} | xargs cat > {1}'.format(infile, outfile), shell=True)

	bin_signal = os.path.join(directory, '*tad.binSignal'.format(prefix))
	for file in glob.glob(bin_signal):
		os.remove(file)

def main():
	# Default Parameters
	arguments = {}
	arguments['chromosome'] = 'assembly'
	arguments['resolution'] = 50000
	arguments['topdom-window'] = 5
	arguments['logging-level'] = 'DEBUG'

	filename = setup().input
	with open(filename) as infile:
		user_set = json.load(infile)
		arguments.update(user_set)

	try:
		arguments['plots'].append(['chr1', 150000000, 160000000])
	except KeyError:
		arguments['plots'] = [['chr1', 150000000, 160000000]]

	logging.basicConfig(level = arguments["logging-level"])
	path = os.path.dirname(os.path.realpath(__file__))
	directory = os.path.dirname(os.path.realpath(filename))

	names, lengths = parse_chromosomes(arguments['species'])

	for i in range(len(names)):
		chromosome = names[i]
		start =  lengths[i]
		end = lengths[i+1]
		size = end - start

		print ('Running analysis for {}'.format(chromosome))

		run_juicer(
			infile = os.path.expanduser(arguments['hic-file']), 
			chromosome = arguments['chromosome'], 
			start = start/2, 
			end = end/2, 
			resolution = arguments['resolution']/2, 
			outfile = os.path.join(directory, '{0}-sparse.txt'.format(arguments['prefix'])), 
			path = path
		)

		run_sparse_to_dense(
			infile = os.path.join(directory, '{0}-sparse.txt'.format(arguments['prefix'])),
			outfile = os.path.join(directory, '{0}-dense.txt'.format(arguments['prefix'])), 
			resolution = arguments['resolution'],
			chromosome = chromosome,
			start = start,
			end = end
		)

		run_topdom(
			infile = os.path.join(directory, '{0}-dense.txt'.format(arguments['prefix'])), 
			outfile = os.path.join(directory, '{0}-{1}-tad'.format(arguments['prefix'], chromosome)), 
			window = str(arguments['topdom-window']), 
			path = path
		)

		for plot in arguments['plots']:
			if plot[0] == chromosome:
				run_plot_tads(
					matrix = os.path.join(directory, '{0}-dense.txt'.format(arguments['prefix'])),
					tad = os.path.join(directory, '{0}-{1}-tad.bed'.format(arguments['prefix'], chromosome)),
					resolution = arguments['resolution'], 
					figure = os.path.join(directory, '{0}-{1}-{2}mb-{3}mb.png'.format(arguments['prefix'], chromosome, int(int(plot[1])/1e6), int(int(plot[2])/1e6))),
					chromosome = plot[0],
					start = int(plot[1]),
					end = int(plot[2])
				)

	cleanup(
		arguments['prefix'],
		directory
	)

if __name__ == '__main__':
	main()

