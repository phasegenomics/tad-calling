import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
from matplotlib import patches 
import numpy as np
import logging

def read(matrix_name, results_name, resolution, start, end):
	logging.debug('Reading TADs')

	start_bin = start/resolution
	end_bin = end/resolution

	matrix = []
	with open(matrix_name) as infile:
		for line in infile:
			row = line.split()[3:]
			matrix.append(row)
	matrix = np.array(matrix, dtype = float)[start_bin:end_bin, start_bin:end_bin]

	domains = []
	with open(results_name) as infile:
		for line in infile:
			split = line.split()
			if len(split) == 4:
				if split[3] == 'domain':
					if int(split[1]) >= start and int(split[2]) <= end:
						i = (int(split[1])/resolution) - start_bin
						j = (int(split[2])/resolution) - start_bin
						domains.append((i, j))

	return matrix, domains

def plot(filename, matrix, domains, resolution, chromosome, start, end):
	logging.debug('Plotting TADs')
	level = logging.getLogger().getEffectiveLevel()
	logging.getLogger().setLevel(logging.INFO)

	plt.figure(figsize = (6, 6), dpi = 200)
	plt.imshow(np.log(matrix + 1), extent = (0, len(matrix), len(matrix), 0))

	for tad in domains:
		x = tad[0]
		y = tad[1]
		size = y - x

		rect = patches.Rectangle((x, y), size, -size, color = 'black', alpha = 0.5)
		plt.gca().add_patch(rect)

	length = int((end - start)/resolution)
	start = int(start/1e6)
	end = int(end/1e6)
	resolution = int(resolution/1e3)

	plt.title('TopDom TAD Calls ({0}kb) Chromosome {1} ({2}mb - {3}mb)'.format(resolution, chromosome, start, end))
	plt.xlabel('Position (mb)')
	plt.ylabel('Position (mb)')
	plt.xticks(np.linspace(0, length, 6), np.linspace(start, end, 6, dtype = int))
	plt.yticks(np.linspace(0, length, 6), np.linspace(start, end, 6, dtype = int))
	plt.savefig(filename, dpi = 200)

	logging.getLogger().setLevel(level)


