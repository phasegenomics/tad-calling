# tad-calling

## Quick Start

python tad-calling/tad-caller.py -i parameters.json

## Parameters Guide

Three required parameters:

1. prefix - all output files will have this prefix, include the path to the folder if you want the results in a folder. 
2. hic-file - path to the .hic file that needs tad calling. (The pipeline expects all chromosomes to be concatenated in a single "assembly" chromosome which is how the .hic files were previously generated. If this changes, some tweaks will need to be made.)
3. species - human or mouse, others can be added by adding a chromosome size file to the defaults folder

Optional:

4. resolution - default is 50kb
5. topdom-window - default is 5
6. logging-level - defaults is debug
