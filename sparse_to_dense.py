import numpy as np
import logging

def read(filename, resolution, start, end):
	logging.debug('Reading input')
 
	bins = ((end-start)/resolution) + 1
	matrix = np.zeros((int(bins), int(bins)))

	with open(filename) as infile:
		for line in infile:
			split = line.split()
			row = ((int(split[0]) * 2) - start)/resolution
			col = ((int(split[1]) * 2) - start)/resolution
			if split[2] == 'NaN':
				count = 0
			else:
				count = float(split[2])

			matrix[row][col] = count 
			matrix[col][row] = count

	return matrix

def write(matrix, filename, resolution, chromosome, start, end):
	bins = (end - start)/resolution + 1

	prefixes = []
	start = 0 
	for i in range(bins):
		end = start + resolution
		prefixes.append('{}\t{}\t{}'.format(chromosome, start, end))
		start = end

	logging.debug('Writing output')

	with open(filename, 'w') as outfile:
		for i in range(len(matrix)):
			line = ''
			for j in range(len(matrix[i])):
				line += '{}\t'.format(matrix[i][j])
			line = '{}\t{}\n'.format(prefixes[i], line.strip())
			outfile.write(line)


